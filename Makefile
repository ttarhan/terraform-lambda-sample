SHELL := /bin/bash

checkenv:
	@if [ -z ${ENV} ]; then\
		echo "ENV was not set"; exit 10;\
	fi

init: checkenv
	@pushd ./${ENV} &&\
	terraform init && \
	popd

plan: init
	@pushd ./${ENV} &&\
	terraform plan -refresh=true \
		-var-file=${ENV}.tfvars &&\
	popd

apply: init
	@pushd ./${ENV} &&\
	terraform apply -refresh=true \
		-var-file=${ENV}.tfvars &&\
	popd

destroy: init
	@pushd ./${ENV} &&\
	terraform destroy -refresh=true \
		-var-file=${ENV}.tfvars &&\
	popd
