variable "account_id" {
  default = ""
}

variable "region" {
  default = "us-west-2"
}

variable "company" {
  default = "sturdy"
}

variable "application" {
  default = "lambda-demo"
}

variable "environment" {
  default = "staging"
}
