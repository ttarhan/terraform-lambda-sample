resource "aws_dynamodb_table" "iot-Sample" {
  name           = "Sample"
  read_capacity  = 1
  write_capacity = 1
  hash_key = "id"

  attribute {
    name = "id"
    type = "S"
  }

  tags {
    Name = "Sample"
    Customer = "${var.company}"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}
