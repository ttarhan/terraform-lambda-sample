
# create zip IF the sha of package.json changes
resource "null_resource" "iot-deps" {
  triggers = {
    package_json = "${base64sha256(file("${path.module}/../lambda-src/helloworld/package.json"))}"
  }
  # Cleanup any exisitng node_modules
  provisioner "local-exec" {
    command = "rm -rf ${path.module}/../lambda-src/helloworld/node_modules"
  }
  # NPM install happens here
  provisioner "local-exec" {
    command = "cd ${path.module}/../lambda-src/helloworld && npm install"
  }
}


# create zip for use by lambda resource below
data "archive_file" "iot-sample" {
  type = "zip"
  source_dir = "${path.module}/../lambda-src/helloworld"
  output_path = "${path.module}/.terraform/files/iot-sample.zip"
  depends_on = [ "null_resource.iot-deps" ]
}


# create aws lambda function
resource "aws_lambda_function" "iot-sample" {
  function_name = "${var.company}-${var.application}-${var.environment}"
  runtime       = "nodejs6.10"
  filename        = "${path.module}/.terraform/files/iot-sample.zip"
  source_code_hash = "${data.archive_file.iot-sample.output_base64sha256}"
  handler       = "index.handler"
  memory_size   = 128
  role          = "${aws_iam_role.lambda_role.arn}"

  environment {
     variables {
        sampleTable = "${aws_dynamodb_table.iot-Sample.name}"
     }
   }
   depends_on = ["data.archive_file.iot-sample"]
}


# create iot-rules cf stack
resource "aws_cloudformation_stack" "sample-dev-SayHelloWorld" {
  name = "tf-demo-sample-dev-SayHelloWorld"

  parameters {
    SayHelloWorld = "sample-dev-SayHelloWorld"
  }
  depends_on = ["aws_lambda_function.iot-sample"]
  template_body = "${file("../templates/lambda-cf.json")}"
}
