# IAM
resource "aws_iam_role" "lambda_role" {
  name               = "${var.company}-lambda-${var.application}-${var.environment}"
  assume_role_policy = "${file("../policies/lambda-role.json")}"
}

# policy & attachment for cw logs
resource "aws_iam_role_policy_attachment" "lambda_log_policy_attachment" {
  role = "${aws_iam_role.lambda_role.name}"
  policy_arn = "${aws_iam_policy.lambda_log_policy.arn}"
}

resource "aws_iam_policy" "lambda_log_policy" {
  name               = "${var.company}-lambda-log-${var.application}-policy-${var.environment}"
  description = "policy to push to cloudwatch logs"
  policy = <<POLICY
{
   "Version": "2012-10-17",
  "Statement": {
    "Action": [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ],
    "Resource": "*",
    "Effect": "Allow",
    "Sid": "LambdaLogAccess"
  }
}
POLICY
}

# policy & attachment for dynamo
resource "aws_iam_role_policy_attachment" "lambda_dynamo_policy_attachment" {
  role = "${aws_iam_role.lambda_role.name}"
  policy_arn = "${aws_iam_policy.lambda_dynamo_policy.arn}"
}

resource "aws_iam_policy" "lambda_dynamo_policy" {
  name = "${var.company}-lambda-dynamo-${var.application}-policy-${var.environment}"
  description = "policy to access dynamo"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": {
    "Action": [
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
      "dynamodb:Query",
      "dynamodb:Scan"
    ],
    "Resource": "arn:aws:dynamodb:${var.region}:${var.account_id}:table/${aws_dynamodb_table.iot-Sample.name}",
    "Effect": "Allow",
    "Sid": "LambdaDynamoAcccess"
  }
}
POLICY
}
