provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "sturdy-tf-state-us-west-2"
    key    = "sturdy-sample-dev-SayHelloWorld/prod/terraform.tfstate"
    region = "us-west-2"
  }
}
